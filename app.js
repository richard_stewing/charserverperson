// basic node.js server with support for web sockets through socket.io
//required modules
var http=require('http');
var io=require('socket.io');
var fs=require('fs');
var couchdb=require('felix-couchdb');

var dbHost="127.0.0.1";
var dbPort=5984;
var dbName='chat';



var client=couchdb.createClient(dbPort, dbHost);
var db=client.db(dbName);

db.exists(function(err, exists){
	if(!exists){
		db.create();
		console.log("db "+dbName+ " created");
	}
	else{
		console.log("db " +dbName+" exists");
	}
});

//required file (such as basic html page, worker js)

var htmlfile=fs.readFileSync('page.html');
var server=http.createServer();
var rooms={};
rooms['test'] = true;


//server answer with standart html file 
server.on('request', function (req, res){
	res.writeHead(200,{'content-type':'text/html'});
	res.end(htmlfile);
});

var socket=io.listen(server);


//web socket connection through socket.io 
socket.on('connection', function(upandrunning){
	//upandrunning.join('test');
	//send data for the worker
	var dialogMe = "";
	var dialogOther = "";
	upandrunning.emit('start');
	//to get data
	var user={};
	upandrunning.on('message', function(data){
		user.to = data.to;
		socket.sockets.in(data.to).emit('newMessage', {mes:data.mes , sender:user.name});
	});
	upandrunning.on('safe', function(data){
		console.log(data);

	});

	upandrunning.on('reqRoom', function(data){
		upandrunning.join(data.room);
		user.room = data.room;
		user.name = data.un;
		if(rooms[data.room]){
			rooms[data.room] = rooms[data.room] +1
			upandrunning.emit('resRoom', { acc:true, resTex:'other client is online'});
		}else{
			rooms[data.room] = 1;
			upandrunning.emit('resRoom', { acc:true, resTex:'no other client is online'});

		}
	});
	upandrunning.on('disconnect', function(){
		upandrunning.leave(user.room);
		rooms[user.room] = rooms[user.room] - 1;
		createDoc({username: user.name, talk: dialogMe, sender: user.to});
		createDoc({username: user.to, talk:dialogMe, sender: user.name});
		if(rooms[user.room]<=0){
			rooms[user.room] = 0;
			console.log("test");
		}  
	});
	
	upandrunning.on('safe', function(data){
		dialogMe = data;
		createDoc({username: user.name, talk:dialogMe, sender: user.to});
		createDoc({username: user.to, talk:dialogMe, sender: user.name});
	});
	upandrunning.on('dialog', function(data){
		getDoc(data.id, data.to, upandrunning);


	});

	
});


//db actions
function createDoc(inf){
	//greates client for the data
	var client=couchdb.createClient(dbPort,dbHost);
	var db= client.db(dbName);

	// creates the object for the data object
	var user={};
	user[inf.sender] = inf.talk;
	// saves doc in database with fallback if err occuers
	db.saveDoc(inf.username,user,function(err, doc){
		if(err){
			console.log(JSON.stringify(err));
			updateData(user, inf.username, inf);
		}else{
			console.log('saved user');
			console.log(doc);
		}
	});
}

//gets data 
function getDoc(to, id, con){
	//creates data
	var client=couchdb.createClient(dbPort,dbHost);
	var db= client.db(dbName);
	
	//gets data with fallback in case of an err of doc is not defined
	db.getDoc(id, function (err, doc){
		if(err){
			console.log(JSON.stringify(err));
		}else{

			if(doc){
				if(doc[to]){
					con.emit('dialog', doc[to]);

				}else{
					con.emit('dialog', "");
				}

			}else{
				
			}
		}
	
	});	
}
function updateData(inf, id, data){
	//updates the user data
	//first needs to get the old doc to get the rev
	var client=couchdb.createClient(dbPort,dbHost);
	var db= client.db(dbName);
	var rev;
	
	db.getDoc(id, function (err, doc){
		if(err){
			console.log(JSON.stringify(err));
		}else{

			if(doc){
				rev = doc._rev;
				console.log(rev);
				var user = doc;
				user[data.sender] = data.talk;
			}
				db.saveDoc(id,user,function(err, doc){
					if(err){
						console.log(JSON.stringify(err));
						console.log(doc);
					}else{
						console.log('saved user');
						console.log(doc);
					}
				});
		}
		
	
	});
	



}






server.listen(8080);
